class LevelTest{
  int d;
  PImage roomSpriteA;
  String room;
  boolean[][] grid = new boolean[31][18];
  
  LevelTest(){
    d = 0; //d = toop
    for(int i = 0; i < 31; i++){
      for(int y = 0; y < 18; y++){
        grid[i][y] = true;
      }
    }
  }//setup
  
  
    void draw(){
    for(int i = 0; i < 31; i++){
      for(int y = 0; y < 18; y++){
        grid[i][y] = true;
      }
    }
    grid[19][1] = false;  
    grid[19][2] = false;
    grid[19][3] = false;
    grid[19][4] = false;
    //grid[10][4] = false;
    //grid[20][5] = false;
    grid[19][7] = false;
    grid[19][8] = false;
    grid[20][1] = false;  
    grid[20][2] = false;
    grid[20][3] = false;
    grid[20][4] = false;
    //grid[10][4] = false;
    //grid[20][5] = false;
    grid[20][7] = false;
    grid[20][8] = false;
   
    grid[11][13] = false;
    grid[12][13] = false;
    grid[13][12] = false;
    grid[14][13] = false;
    grid[15][13] = false;
    grid[16][14] = false;
    
    if(d == 0){
        roomSpriteA = loadImage("data/s_level_test0.png");
        //roomSpriteA = loadImage("data/s_level_test1.png");
        roomSpriteA.resize(ssx,ssy);
      d = 3;
    }
      image(roomSpriteA, 0, 0);
          
      for(int i = 0; i < 30; i++){
        for(int y = 0; y < 17; y++){
          if(!grid[i][y]){
            fill(255, 255, 255, 125);
            rect((i) * 32, (y) * 32, 32, 32);
            fill(255);
          }
        }
      }
  }
  
}
