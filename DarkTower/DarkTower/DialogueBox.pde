class DialogueBox{
  PImage boxImage;
  int toop;
  String optionA, optionB, optionC;
  PImage portraitImage;
  PImage portrait;
  
  void setup(){
    boxImage = loadImage("s_dialogue_box.png");
    portrait = loadImage("s_npc_frog_portrait_0.png");
    
  }
  
  DialogueBox(){
    toop = 0;
    optionA = "option A";
    optionB = "option B";
    optionC = "option C";
    boxImage = loadImage("s_dialogue_box.png"); //This is the problem, it cant find any images.
    portrait = loadImage("s_npc_frog_portrait_0.png");  
}
  
  void draw(){
    if(toop == 0){
      boxImage = loadImage("s_dialogue_box.png");
      portrait = loadImage("s_npc_frog_portrait_0.png");

      toop = 1;
      
    }
    image(boxImage,0,0);
    image(portrait,(displayWidth/2)+(portrait.width/2)+12,6); //CHANGE TO: image(portrait,displayWidth-portrait.width,6);
         
  }
  void setText(String a, String b, String c){
    optionA = a;
    optionB = b;
    optionC = c;
  }
}
