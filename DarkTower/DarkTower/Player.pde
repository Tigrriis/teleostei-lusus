class Player{
  int px; //x position
  int py; //y position
  
  PImage spriteWalkLeft, spriteWalkRight, spriteIdleRight;
  int d;
  String direction;
  boolean gridFree;
  int c;
  int rc, lc;
  int speed = ssx / ( 1920 / 6 );
  int desiredX, desiredY;
  int stripLength;
  boolean movingRight;
  boolean movingLeft;
  boolean movingUp;
  boolean movingDown;
  boolean debugMode;
  color detectRight, detectLeft, detectUp, detectDown;
  color black;
  
  Player (){ //constructor of the class, runs once
    px = 64;
    py = 64;
    desiredX = 128;
    desiredY = 128;
    d = 0;
    stripLength = 4;
    movingRight = false;
    movingLeft = false;
    movingUp = false;
    movingDown = false;
    c=0;
    black = (0);
    debugMode = true;
    gridFree = true;
  }
  
  void setup(){
        spriteWalkLeft = loadImage("s_player_walk_left.png");
        spriteWalkRight = loadImage("s_player_walk_right.png");
        spriteIdleRight = loadImage("s_player_walk_right.png");
  }
  
  void draw(){
    if(d == 0){
        //spriteWalkLeft = loadImage("s_player_walk_left.png");
        //spriteWalkRight = loadImage("s_player_walk_right.png");
        //spriteIdleRight = loadImage("s_player_walk_right.png");
      d = 1;
      direction = "right";
    }
<<<<<<< HEAD
    if(movingRight){
      image(spriteWalkRight.get(frameCount/10%4*(spriteWalkRight.width/stripLength), 0, spriteWalkRight.width/stripLength, spriteWalkRight.height), px - 32, py - 36);
    }else if(movingLeft){
      image(spriteWalkLeft.get(frameCount/10%4*(spriteWalkLeft.width/stripLength), 0, spriteWalkLeft.width/stripLength, spriteWalkLeft.height), px - 32, py - 36);
    }else if(movingUp){
      image(spriteWalkLeft.get(frameCount/10%4*(spriteWalkLeft.width/stripLength), 0, spriteWalkLeft.width/stripLength, spriteWalkLeft.height), px - 32, py - 36);
    }else if(movingDown){
      //image(spriteWalkLeft.get(frameCount/10%4*(spriteWalkLeft.width/stripLength), 0, spriteWalkLeft.width/stripLength, spriteWalkLeft.height), px - 32, py - 36);
    }else{
      //image(spriteWalkLeft.get(frameCount/10%4*(spriteIdleRight.width/stripLength), 0, spriteIdleRight.width/stripLength, spriteIdleRight.height), px - 32, py - 36);
    }
    //image(sprite[c], px - 32, py - 36);// draw the player's sprite
    if(movingRight && detectRight != -16777216 && px == desiredX){
=======
     if(frameCount%speed == 0){
       if(dD && px < width - 64){
         if(c < 3){
           c++;
       }else{
         c=0;
       }
     }else if(aD){
         if(c < 7 && c > 3 && px > 64){
           c++;
       }else{
         c=4;
       }
     }
     else if(uD){
         if(c < 7 && c > 3 && py > 64){
           c++;
       }else{
         c=4;
       }
     }
     else if(vD){
         if(c < 7 && c > 3 && py < width - 64){
           c++;
       }else{
         c=4;
       }
     }
     }
    image(sprite[c], px - 32, py - 36);// draw the player's sprite
    if(dD && detectRight != -16777216 && px == desiredX){
>>>>>>> f831cb00ad5288962ce98ad4a3321a9c23cdd6c0
      desiredX += 32;
    }
    if(movingLeft && detectLeft != -16777216 && px == desiredX){
      desiredX -= 32;
    }
    if(movingUp && detectUp != -16777216 && py == desiredY){
      desiredY -= 32;
    }
    if(movingDown && detectDown != -16777216 && py == desiredY){
      desiredY += 32;
    }

    if(levelTest.grid[desiredX / 32][desiredY / 32] && levelTest.grid[(desiredX + 32) / 32][(desiredY + 32) / 32] && levelTest.grid[(desiredX - 32) / 32][(desiredY - 32) / 32]){
      gridFree = true;
    }else{
      gridFree = false;
    }
    
    if (desiredX > px && gridFree){
      px+=4;
    }
    if (desiredX < px && gridFree){
      px-=4;
    }
    if (desiredY > py && gridFree){
      py+=4;
    }
    if (desiredY < py && gridFree){
      py-=4;
    }
    if(gridFree != true){
      desiredX = px;
      desiredY = py;
    }
    
    detectDown = get(px, py + 35);
    detectUp = get(px, py - 35);
    detectRight = get(px + 35, py);
    detectLeft = get(px - 35, py);
    
    if(debugMode){
      stroke(255,0,0);
      strokeWeight(4);
      point(px, py + 35);
      point(px, py - 35);
      point(px + 35, py);
      point(px - 35, py);
      
      //
      point(px, py);
      //
      
      textAlign(LEFT);
      text(str(desiredX), 16, 16);
      text(str(desiredY), 16, 32);
      text(str(px), 16, 48);
      text(str(py), 16, 64);
      text(str(detectLeft), 16, 80);
      text(str(detectRight), 16, 96);
      text(str(detectUp), 16, 118);
      text(str(detectDown), 16, 130);
      text(str(levelTest.grid[desiredX / 64][desiredY / 64]), 16, 300);
      //for(int i = 0; i < 30; i++){
        //for(int o = 0; o < 17; o++){
          //text(str(levelTest.grid[i][o]), (i*30) + 50, (o*12) + 20);
        //}
      //}
    }
  }

  void moveRight(){
    movingRight = true; 
  }
  void moveLeft(){
    movingLeft = true;
  }
  void releaseRight(){
    movingRight = false;
  }
  void releaseLeft(){
    movingLeft = false;
  }
  
  void moveUp(){
    movingUp = true;
  }
  void moveDown(){
    movingDown = true;
  }
  void releaseUp(){
    movingUp = false;
  }
  void releaseDown(){
    movingDown = false;
  }
  void mouseReleased(){
    movingRight = false;
    movingLeft = false;
  }
}
