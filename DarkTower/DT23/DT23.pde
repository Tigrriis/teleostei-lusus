
//Version number????

//Btw, P to enter chat, O to leave, and I to change Pepe face.


int mode = 1;
PImage pepePortraitSad;
PImage pepePortraitNeutral;

boolean debugMode;
Map map = new Map();
Player player = new Player();
DialogueBox dialogueBox = new DialogueBox();

void setup(){
  debugMode = false;
  size(int(map.screenSize.x), int(map.screenSize.y));
  pepePortraitNeutral = loadImage("s_npc_frog_portrait_0.png");
  pepePortraitSad = loadImage("s_npc_frog_portrait_1.png");
}
void draw(){
  
  if(mode == 1){
  map.draw();
  player.draw();
  

    if (key == 'p'){    //enters dialogue
        dialogueBox.toop = 1;
        mode = 2;
  }
  
  }
  
  if(mode == 2){
    dialogueBox.draw();
    
    
    if (key == 'o'){      //closes dialogue
      mode = 1;
      dialogueBox.portraitImage = pepePortraitNeutral;
    }
    if (key == 'i'){      //Changes Pepe Portrait (Testi)
      dialogueBox.portraitImage = pepePortraitSad;
    }

  if(debugMode){
    //println(map.rowNumber);
    //println(map.gridDivisor.x);
    //println(map.gridDivisor.y);

  }
} 

void keyPressed(){
  if (key == 'w'){

    player.move("up");
  }
  if (key == 'a'){

    player.move("left");
  }
  if (key == 's'){

    player.move("down");
  }
  if(key == 'd'){
    player.move("right");
  }

}
