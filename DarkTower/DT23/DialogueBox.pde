class DialogueBox{
  PImage boxImage;
  PImage portraitImage;
  int toop = 1;
  
  void setup(){

  }
  
  DialogueBox(){

  }
  
  void draw(){
    
    if(toop == 1){
        imageMode(CORNER);
        boxImage = loadImage("s_dialogue_box.png");
        portraitImage = loadImage("s_npc_frog_portrait_0.png");
        image(boxImage,0,0);
        toop = 0;
    }
    image(portraitImage,(map.screenSize.x-portraitImage.width-6),6);
  }
}
