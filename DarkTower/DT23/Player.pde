class Player{
  int stripLength;
  boolean toop;
  PVector pixelPos;
  PVector gridPos;
  PImage spriteWalkLeft;
  
  Player(){
    toop = false;
    stripLength = 4;
    gridPos = new PVector(3,3);
    pixelPos = new PVector(0,0);     
  }
  
  void draw(){
    if(!toop){
      spriteWalkLeft = loadImage("s_player_walk_left.png");
      toop = true;
    }
    pixelPos = new PVector(gridPos.x * map.gridSize + (map.gridSize/2), gridPos.y * map.gridSize + (map.gridSize/2));
    noStroke();
    fill(255, 0, 0);
    rectMode(CENTER);
    //rect(pixelPos.x, pixelPos.y, map.gridSize/2, map.gridSize/2);
    imageMode(CENTER);
    image(spriteWalkLeft.get(frameCount/4%4*(spriteWalkLeft.width/stripLength), 0, spriteWalkLeft.width/stripLength, spriteWalkLeft.height), pixelPos.x, pixelPos.y, map.gridSize * 2, map.gridSize);
<<<<<<< HEAD
    
    
=======
>>>>>>> c328227ce3dfce74010480f1bfe5fb00d2b53af0
  }
  void move(String direction){
    if(direction == "right"){
      if(map.canMove(int(gridPos.x + 1), int(gridPos.y))){
        gridPos.x++;
      }
    }
    if(direction == "left"){
      if(map.canMove(int(gridPos.x - 1), int(gridPos.y))){
        gridPos.x--;
      }
    }
    if(direction == "up"){
      if(map.canMove(int(gridPos.x), int(gridPos.y - 1))){
        gridPos.y--;
      }
    }
    if(direction == "down"){
      if(map.canMove(int(gridPos.x), int(gridPos.y + 1))){
        gridPos.y++;
      }
    }
  }
}
