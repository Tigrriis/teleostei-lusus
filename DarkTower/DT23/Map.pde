class Map{
  PImage tiles[] = new PImage[255];
  PVector screenSize;
  PVector mapSize;
  float gridSize;
  Table docksLeft;
  boolean toop;
  int rowNumber;
  int stripLength;
  PVector gridDivisor;
  TableRow deathRow;
  
  Map(){
    gridDivisor = new PVector(15, 8);
    screenSize = new PVector(960, 540);
    gridSize = int(screenSize.x) / gridDivisor.x;
    mapSize = new PVector(gridDivisor.x, gridDivisor.y);
    toop = false;
    rowNumber = 0;
    stripLength = 1;
  }
  void draw(){
    if(!toop){
      tiles[0] = loadImage("s_wood.png");
      tiles[1] = loadImage("s_barrel.png");
      tiles[2] = loadImage("s_water.png");
      tiles[30] = loadImage("s_door_fade_wood_left.png");
      tiles[31] = loadImage("s_door_fade_wood_right.png");
      docksLeft = loadTable("m_docks_left.csv");
      toop = true;
    }
    
    background(0);
    stroke(127);
    noFill();
    rectMode(CORNER);
    // --- Drawing the grid
    for(TableRow row : docksLeft.rows()){
      for(int i = 0; i < mapSize.x; i++){
        imageMode(CORNER);
        //println(rowNumber);
        //println(gridDivisor.y);
        if(rowNumber > gridDivisor.y){
          gridDivisor = new PVector(rowNumber * 1.875, rowNumber);
          gridSize = int(screenSize.x) / gridDivisor.x;
          mapSize = new PVector(gridDivisor.x, gridDivisor.y);
        }
        if(row.getInt(i) == 2){
          stripLength = 3;
        }else{
          stripLength = 1;
        }
        image(tiles[row.getInt(i)].get(frameCount/20%stripLength*(tiles[row.getInt(i)].width/stripLength), 0, tiles[row.getInt(i)].width/stripLength, tiles[row.getInt(i)].height), i * gridSize, rowNumber * gridSize, gridSize, gridSize);
      }
      rowNumber++;
    }
    rowNumber = 0;
  } 
  boolean canMove(int x, int y){
    deathRow = docksLeft.getRow(y);
    println(str(deathRow.getInt(x)));
    if(deathRow.getInt(x) == 1 || deathRow.getInt(x) == 2){
      return(false);
    }else{
    return(true);
    }
  }
}
