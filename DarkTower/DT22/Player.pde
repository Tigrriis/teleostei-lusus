class Player {
  int stripLength;
  String tickleBoo;
  String direction;
  boolean toop;
  PVector pixelPos;
  PVector gridPos;
  PImage spriteWalkLeft, spriteIdleLeft;
  PImage spriteWalkRight, spriteIdleRight;
  PImage spriteActive;

  Player() {
    toop = false;
    stripLength = 4;
    gridPos = new PVector(3, 3);
    pixelPos = new PVector(0, 0);     
    pixelPos = new PVector(gridPos.x * map.gridSize + (map.gridSize/2), gridPos.y * map.gridSize + (map.gridSize/2));
  }

  void draw() {
    if (!toop) {
      spriteWalkLeft = loadImage("s_player_walk_left.png");
      spriteWalkRight = loadImage("s_player_walk_right.png");
      spriteIdleLeft = loadImage("s_player_idle_left.png");
      spriteIdleRight = loadImage("s_player_idle_right.png");
      spriteActive = loadImage("s_player_walk_left.png");
      tickleBoo = "left";
      toop = true;
    }
    if (pixelPos.x < gridPos.x * map.gridSize + (map.gridSize / 2)) {
      pixelPos.x+=4;
      stripLength = 4;
      spriteActive = spriteWalkRight;
      tickleBoo = "right";
    }
    if (pixelPos.x > gridPos.x * map.gridSize + (map.gridSize / 2)) {
      pixelPos.x-=4;
      stripLength = 4;
      spriteActive = spriteWalkLeft;
      tickleBoo = "left";
    }
    if (direction == "right" && pixelPos.x == gridPos.x * map.gridSize + (map.gridSize / 2)) {
<<<<<<< HEAD
      if (map.canMove(int(gridPos.x + 1), int(gridPos.y), 0)) {
=======
      if (map.canMove(int(gridPos.x + 1), int(gridPos.y))) {
>>>>>>> e422292e8319e0a6f92dda258573c83821458cc3
        gridPos.x++;
        //spriteActive = spriteWalkRight;
      }
    }
    if (direction == "left" && pixelPos.x == gridPos.x * map.gridSize + (map.gridSize / 2)) {
<<<<<<< HEAD
      if (map.canMove(int(gridPos.x - 1), int(gridPos.y), 0)) {
=======
      if (map.canMove(int(gridPos.x - 1), int(gridPos.y))) {
>>>>>>> e422292e8319e0a6f92dda258573c83821458cc3
        gridPos.x--;
        //spriteActive = spriteWalkLeft;
      }
    }
    if (direction == "up" && pixelPos.y == gridPos.y * map.gridSize + (map.gridSize / 2)) {
<<<<<<< HEAD
      if (map.canMove(int(gridPos.x), int(gridPos.y - 1), 0)) {
=======
      if (map.canMove(int(gridPos.x), int(gridPos.y - 1))) {
>>>>>>> e422292e8319e0a6f92dda258573c83821458cc3
        gridPos.y--;
      }
    }
    if (direction == "down" && pixelPos.y == gridPos.y * map.gridSize + (map.gridSize / 2)) {
<<<<<<< HEAD
      if (map.canMove(int(gridPos.x), int(gridPos.y + 1), 0)) {
=======
      if (map.canMove(int(gridPos.x), int(gridPos.y + 1))) {
>>>>>>> e422292e8319e0a6f92dda258573c83821458cc3
        gridPos.y++;
      }
    }
    if (pixelPos.y < gridPos.y * map.gridSize + (map.gridSize / 2)) {
      pixelPos.y+=4;
    }
    if (pixelPos.y > gridPos.y * map.gridSize + (map.gridSize / 2)) {
      pixelPos.y-=4;
    }
    if (pixelPos.x == gridPos.x * map.gridSize + (map.gridSize / 2)) {
      if (tickleBoo == "right") {
        stripLength = 1;
        spriteActive = spriteIdleRight;
      }
      if (tickleBoo == "left") {
        stripLength = 1;
        spriteActive = spriteIdleLeft;
      }
    }
    noStroke();
    fill(255, 0, 0);
    rectMode(CENTER);
    //rect(pixelPos.x, pixelPos.y, map.gridSize/2, map.gridSize/2);
    imageMode(CENTER);
    image(spriteActive.get(frameCount/4%stripLength*(spriteActive.width/stripLength), 0, spriteActive.width/stripLength, spriteActive.height), pixelPos.x, pixelPos.y, map.gridSize * 2, map.gridSize);
  }
}

