class Bullet {
  float m;

  boolean toop;
  boolean active;

  String direction;
  PVector desired;
  PVector position;

  PImage bulletSprite;
  Bullet() {
    m = 0;

    direction = "left";
    active = false;
    toop = true;
  }
  void draw() {
    if (toop) {
      position = new PVector(0, 0);
      bulletSprite = loadImage("s_bullet.png");
      toop = false;
    }
if(active){
    if (position.x > 1 && position.y > 1 && position.x < map.screenSize.x - 1 && position.y < map.screenSize.y - 1 && map.canMove(int(position.x / map.gridSize), int(position.y / map.gridSize), 5)) {
      if (direction == "left") {
        position.x -= 14;
      } else if (direction == "right") {
        position.x += 14;
      }
    } else {
     //position.x = -8;
      //position.y = -8;
      active = false;
    }

    image(bulletSprite, position.x, position.y, map.gridSize / 4, map.gridSize / 4);
  }else{
        position.x = 64;
      position.y = 64;
  }
}
  void fire(int x, int y, String dir) {
    active = true;
    position = new PVector(player.pixelPos.x, player.pixelPos.y);
    direction = dir;
  }
}

